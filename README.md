# MasterFlix website template
Stunning website with a good UI design for the MasterFlix Discord bot, built with Bootstrap.

## Built With:

* [Bootstrap](https://getbootstrap.com/) - The web framework used
* [Jquery](https://jquery.com/) - For scripts

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.